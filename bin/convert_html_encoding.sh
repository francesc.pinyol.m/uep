#!/bin/bash

# defaults
output_encoding=utf-8
dry_run=0

function print_help_and_exit {
    cat <<EOF
Usage: `basename $0` [options] html_filename

Converts html_filename to ${output_encoding}, by modifying the file text encoding and setting:
  <meta content="text/html; charset=${output_encoding}" http-equiv="Content-Type">

Options:
  --dry-run     only simulation
EOF
    exit 1
}


MIN_ARGS=1
MAX_ARGS=3
if (( $# < $MIN_ARGS )) || (( $# > $MAX_ARGS ))
then
    print_help_and_exit
fi

# options
if ! params=$(getopt -o vh --long verbose,help,dry-run -n $0 -- "$@")
then
    # invalid option
    print_help_and_exit
fi
eval set -- ${params}

while true
do
    case "$1" in
        -h | --help ) print_help_and_exit;;
        -v | --verbose ) verbose=1; shift ;;
        --dry-run ) dry_run=1; shift ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
done

# parameters
input_path=$1

input_basename=$(basename ${input_path})
input_name=${input_basename%.*}
input_extension=${input_basename##*.}
input_dirname=$(dirname ${input_path})

output_basename=${input_name}.${output_encoding}.${input_extension}
output_path=${input_dirname}/${output_basename}

# get present mime-type
mime=$(file -bi ${input_path})
#echo "file mime: ${mime}"

# get present text encoding charset
charset=${mime##*=}
echo -n "${input_path}: charset=${charset}"


if [[ $charset != $output_encoding ]]
then
    echo " -> converting file to $output_encoding"

    if ! (( dry_run ))
    then
	# convert file encoding
	iconv -f ${charset} -t ${output_encoding} ${input_path} >${output_path}
	
	# convert html charset
	sed -i "/<meta.*charset/ c\  <meta content=\"text/html; charset=${output_encoding}\" http-equiv=\"Content-Type\">" ${output_path}
	
	# rename output file
	rm -f ${input_path}
	mv ${output_path} ${input_path}
    fi
else
    echo " -> file already in $output_encoding"
fi

exit 0
